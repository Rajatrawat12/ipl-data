const fs = require("fs")

//.....................question 1......................................
function matchesPlayedPerYear(matches){
    const result ={}
    for(let match of matches){
        let season =match.season
        if(result[season]){
            result[season]+=1
        } else {
            result[season]=1
        }
    }
   // return result
    fs.writeFileSync("/home/rajat/src/public/output/matchesPlayedPerYear.json",JSON.stringify(result,null,2))
}



//...............................question2.............................
function matchesWonPerYear(matches) {
    let wins = {}
    for (let match of matches) {
      if (wins[match.season]) {
        wins[match.season][match.winner] += 1
      }
  
     else {
        let team = {}
        for (let item of matches) {
          team[item.winner] = 1
        }
        wins[match.season] = team
      }
    }

    
    //return wins
    fs.writeFileSync("/home/rajat/src/public/output/matchesWonPerYear.json",JSON.stringify(wins,null,2))
  
}


//....................................question 3.................................................
function extraRunConceded2016(matches,deliveries){
    let result={}
  for(let match of matches){
    if(match.season== 2016){
    for(let delivery of deliveries){
if(match.id===delivery.match_id){
    let extra_runs =delivery.extra_runs
    let team=delivery.bowling_team
    if(result[team]){
        result[team]+= parseInt(extra_runs)
    } else {
        result[team]=parseInt(extra_runs)
        }
    }
    }
    }
}
//return result
fs.writeFileSync("/home/rajat/src/public/output/extraRunConceded2016.json",JSON.stringify(result,null,2))
    }


    //....................................question 4.....................................
    function mostEconomicalBowler2015(matches,deliveries) {

        let BowlerData = {}
        for(match of matches){
            if (match.season == 2015) {
               
                for (let delivery of deliveries) {
            
                    if (match.id ==delivery.match_id) {
    
                        if (BowlerData[delivery.bowler]) {
                            BowlerData[delivery.bowler].balls += 1
                            BowlerData[delivery.bowler].runs += parseInt(delivery.total_runs)
                        } else {
                            let temp = {}
                            temp.balls = 1
                            temp.runs = parseInt(delivery.total_runs)
                            BowlerData[delivery.bowler] = temp
                        }
                    }
                }
            } 
    }
       
        let arrayOfEachPlayerEconamy = []
      
        for (let item in BowlerData) {
            let eachPlayerEconomy = {}
            let numberOfOvers = (BowlerData[item].balls / 6)
            let economy = (BowlerData[item].runs / numberOfOvers) 
            eachPlayerEconomy[item] = economy
            arrayOfEachPlayerEconamy.push(eachPlayerEconomy)
        }
    
      
        let sortedArray = arrayOfEachPlayerEconamy.sort((a, b) => a[Object.keys(a)] - b[Object.keys(b)])
    
    let topTenPlayer=sortedArray.splice(0,10)
    //return topTenPlayer
    fs.writeFileSync("/home/rajat/src/public/output/mostEconomicalBowler2015.json",JSON.stringify(topTenPlayer,null,2))
    }
    //.................................question 5.............................
    function matchWinToss(matches){
        let result={}
        for(let match of matches){
            let toss= match.toss_winner
            let winner=match.winner
            if(match[toss]===match[winner]){
               if (result[winner]){
                result[winner]+=1
               }else {
                result[winner]=1
               } }
    
               }
              // return result
              fs.writeFileSync("/home/rajat/src/public/output/matchWinToss.json",JSON.stringify(result,null,2))
            }
    
            
            //.........................question 6................................
            function manOfTheMatchPerYear(matches) {
                let result = {}
                for (let match of matches) {
                  if (result[match.season]) {
                    result[match.season][match.player_of_match] += 1
                  } else {
                    let temp = {}
                    for (let item of matches) {
                      temp[item.player_of_match] = 1
                    }
              
                    result[match.season] = temp
                  }
                }
                //return  Object.entries(result)
                
                
                let yearss = Object.entries(result).map(year =>
                  Object.entries(year[1]).sort((a, b) => b[1] - a[1])
                
                )
               //return yearss
              
                
              
              //return Object.keys(result)
              
              let years = [];
              let players = [];
              
              Object.keys(result).map(year => {
                years.push(year)
              })
              
              
              
              yearss.map(season => {
                players.push(season[0][0])
              })
              
              //return players
              
              let final = {};
              
              
              for(let i=0 ;i<years.length ;i++){
                final[years[i]]=players[i]
              }
              
              //return final
              fs.writeFileSync("/home/rajat/src/public/output/manOfTheMatchPerYear.json",JSON.stringify(final,null,2))
              }
              
              
//..................................question 7.........................................
function strikeRatePerSeason (matches,deliveries){
    const player = deliveries.filter( PlayerData=>PlayerData.batsman === "S Dhawan")
    

    let data = [];
    for(let item of player){
        let years = matches.filter(match => match.id === item.match_id)
        //return years
        for(let i =0;i<years.length; i++){
            item.season = years[i].season
            //console.log(item.season)
            data.push(item);
        }

    }
    
 let strikeRate = data.reduce((acc,curr)=> {
        if(curr.match_id){
            if(acc[curr.season]){
                acc[curr.season]["runs"]+= parseInt(curr.batsman_runs)
                acc[curr.season]["balls"]++
                acc[curr.season]["strikRate"] = parseFloat((acc[curr.season].runs/acc[curr.season].balls)*100).toFixed(3)
            } else {
                acc[curr.season] = {}
                acc[curr.season]["runs"] = parseInt(curr.batsman_runs)
                acc[curr.season]["balls"] = 1
                }
            }

       return acc;

    },{})


   // return {"S Dhawan": Object.fromEntries(Object.entries(strikeRate).map(each => [each[0],each[1].strikRate]))

   fs.writeFileSync("/home/rajat/src/public/output/strikeRatePerSeason.json",JSON.stringify(strikeRate,null,2))

}
 
//........................question 8.........................      
function highestDismissal(deliveries){
    let res={}

    for(let delivery of deliveries){
        if((delivery.player_dismissed!=="")& (delivery.dismissal_kind!=="run out")){
            let bowler=delivery.bowler
            let batsman=delivery.batsman
            let str=batsman+"-"+bowler

           if(res[str]){
                res[str]+=1
            }else{
                res[str] =1
               
            }
        }
    }

    //return res
    let array=Object.entries(res)
//return array
let sort=array.sort((a,b)=>b[1]-a[1])
//return sort
let final=sort.slice(0,1)
//return Object.fromEntries(final)
fs.writeFileSync("/home/rajat/src/public/output/highestDismissal.json",JSON.stringify(final,null,2))
}
//...............................question 9..........................................
function bestEconomyInSuperOver(deliveries) {
    let result={}
    for(let delivery of deliveries){
        if((delivery.is_super_over!=="0")&(delivery.dismissal_kind!=="run out")){
            //let bowler=delivery.bowler
           // let run=delivery.total_runs
            if(result[delivery.bowler]){
                result[delivery.bowler].balls +=1
                result[delivery.bowler].runs+=parseInt(delivery.total_runs)
            }
            else {
                let temp= {}
                 temp.balls= 1
                 temp.runs= parseInt(delivery.total_runs)
                 result[delivery.bowler]= temp
        }
    }
}
//return result
let arrayOfEachPlayerEconamy=[]
//let eachPlayerEconomy={}
for(let item in result){
    let eachPlayerEconomy={}
    let overs=(result[item].balls/6)
let economy=(result[item].runs/overs)
eachPlayerEconomy[item]=economy
arrayOfEachPlayerEconamy.push(eachPlayerEconomy)
}
//return arrayOfEachPlayerEconamy

let sortedArray = arrayOfEachPlayerEconamy.sort((a, b) => a[Object.keys(a)] - b[Object.keys(b)])
//return sortedArray

let topeconomy= sortedArray.splice(0,1)
//return topeconomy
fs.writeFileSync("/home/rajat/src/public/output/bestEconomyInSuperOver.json",JSON.stringify(topeconomy,null,2))
}




module.exports ={
    matchesPlayedPerYear:matchesPlayedPerYear,
    matchesWonPerYear:matchesWonPerYear,
    extraRunConceded2016:extraRunConceded2016,
    mostEconomicalBowler2015:mostEconomicalBowler2015,
    matchWinToss:matchWinToss,
    manOfTheMatchPerYear:manOfTheMatchPerYear,
    strikeRatePerSeason :strikeRatePerSeason ,
    highestDismissal:highestDismissal,
    bestEconomyInSuperOver:bestEconomyInSuperOver
}

